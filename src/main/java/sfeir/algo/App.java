package sfeir.algo;


import java.io.File;
import java.io.FileNotFoundException;
import java.nio.file.Paths;
import java.util.Map;

import static java.util.Objects.isNull;

/**
 * @author airhacks.com
 */
public class App {

    public static void main(String[] args) throws FileNotFoundException {
        System.out.printf("App has started with arguments: %s%s", String.join(", ", args), System.lineSeparator());
        CmdsParser parser = new CmdsParser(System.out, args);
        Map<Commands, String> cmds = parser.parse();
        if (isNull(cmds) || cmds.isEmpty()) {
            // give help
            return;
        }

        if (cmds.containsKey(Commands.FILE)) {
            new Tail(System.out, new File(Paths.get(cmds.get(Commands.FILE)).toUri()), Integer.parseInt(cmds.get(Commands.DISPLAY)));
        }
    }
}
