package sfeir.algo;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Deque;
import java.util.List;
import java.util.stream.Collectors;

import static java.util.Objects.nonNull;

public class Tail {

    private final PrintStream logger;
    private final File file;
    private final Integer linesToShow;
    private final static Integer DEFAULT_LINES_TO_SHOW = 1;

    /**
     * Expected format args: ./tail --file=x.txt --display=3
     */
    public Tail(PrintStream logger, File file, Integer linesToShow) {
        this.logger = logger;
        this.file = file;
        this.linesToShow = nonNull(linesToShow) ? linesToShow : DEFAULT_LINES_TO_SHOW;
        //this.logger.printf("Start tail with file: %s and with %s lines to show%s", file.getName(), this.linesToShow, System.lineSeparator());
    }

    public String run() throws IOException {
        BufferedReader br = new BufferedReader(new FileReader(file, StandardCharsets.UTF_8));

        Deque<String> stack = new ArrayDeque<>();

        String line = br.readLine();
        stack.add(line);

        while (nonNull(line)){
            line = br.readLine();

            if(nonNull(line)){
                stack.add(line);
            }

            if(stack.size() > this.linesToShow){
                stack.remove();
            }
        }

        String resultAsString = String.join(System.lineSeparator(), stack.stream().collect(Collectors.joining(System.lineSeparator())));
        logger.println(resultAsString);

        return resultAsString;
    }

    public Integer getLinesToShow() {
        return linesToShow;
    }

}
