package sfeir.algo;

import java.io.PrintStream;
import java.util.*;

import static java.util.Objects.isNull;

public class CmdsParser {

    private final PrintStream logger;
    private final List<String> args;

    public CmdsParser(PrintStream logger, String[] args) {
        this.logger = logger;
        this.args = List.of(args);
        this.logger.printf("Start parser with arguments: %s%s", String.join(", ", args), System.lineSeparator());
    }

    public Map<Commands, String> parse() {
        if(isNull(args) || args.isEmpty()){
            return null;
        }

        Map<Commands, String> cmds = new HashMap<>();
        for (String arg : args) {
            if (arg.startsWith("--")) {
                String[] splittedArg = arg.split("=");
                if (splittedArg.length == 2) {
                    String cmd = splittedArg[0].substring(splittedArg[0].lastIndexOf("-") + 1);
                    String value = splittedArg[1];
                    switch (Commands.valueOf(cmd.toUpperCase(Locale.ROOT))) {
                        case FILE -> cmds.put(Commands.FILE, value);
                        case DISPLAY -> cmds.put(Commands.DISPLAY, value);
                    }
                }
            } else {
                throw new IllegalArgumentException(String.format("Unrecognised argument with value %s", arg));
            }
        }
        return cmds;
    }
}
