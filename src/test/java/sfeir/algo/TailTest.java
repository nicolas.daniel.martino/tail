package sfeir.algo;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.*;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.charset.StandardCharsets;

public class TailTest {

    PrintStream testLogger;
    ByteArrayOutputStream loggerOutput;
    final String utf8 = StandardCharsets.UTF_8.name();

    @BeforeEach
    public void setup() {
        this.loggerOutput = new ByteArrayOutputStream();
        this.testLogger = new PrintStream(loggerOutput);
    }

    @Test
    public void shouldShowAllIfLinesToShowAreBiggerThanTotalLinesInFiles() throws URISyntaxException, IOException {
        //given
        URL fileUrl = getClass().getClassLoader().getResource("testfile.txt");
        Assertions.assertNotNull(fileUrl);
        int linesToShow = 1000;
        Tail tail = new Tail(testLogger, new File(fileUrl.toURI()), linesToShow);

        //when
        String result = tail.run();
        //then
        String expected = String.format("this is a test file%1$sit should allow to test the tail program%1$sand test it well", System.lineSeparator());
        Assertions.assertEquals(expected, result);
    }

    @Test
    public void shouldReturnCorrectTail() throws IOException, URISyntaxException {
        //given
        URL fileUrl = getClass().getClassLoader().getResource("testfile.txt");
        Assertions.assertNotNull(fileUrl);
        Tail tail = new Tail(testLogger, new File(fileUrl.toURI()), 1);

        //when
        String result = tail.run();
        //then

        Assertions.assertEquals("and test it well", result);
    }

    @Test
    public void shouldLogCorrectTail() throws IOException, URISyntaxException {
        //given
        URL fileUrl = getClass().getClassLoader().getResource("testfile.txt");
        Assertions.assertNotNull(fileUrl);
        Tail tail = new Tail(testLogger, new File(fileUrl.toURI()), 2);

        //when
        String result = tail.run();
        //then

        String expected = String.format("it should allow to test the tail program%1$sand test it well%1$s", System.lineSeparator());
        Assertions.assertEquals(expected, loggerOutput.toString(utf8));
    }
}
