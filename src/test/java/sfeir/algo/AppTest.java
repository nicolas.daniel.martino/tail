package sfeir.algo;

import org.junit.jupiter.api.Test;

import java.io.FileNotFoundException;

public class AppTest {

   @Test
   public void mainWithoutParameters() throws FileNotFoundException {
        var parameters = new String[]{};
       App.main(parameters);
   }

}
