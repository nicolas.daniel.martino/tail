package sfeir.algo;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.io.UnsupportedEncodingException;
import java.nio.charset.StandardCharsets;
import java.util.Map;

public class CmdParserTest {

    PrintStream testLogger;
    ByteArrayOutputStream loggerOutput;
    final String utf8 = StandardCharsets.UTF_8.name();

    @BeforeEach
    public void setup() {
        this.loggerOutput = new ByteArrayOutputStream();
        this.testLogger = new PrintStream(loggerOutput);
    }

    @Test
    public void testLog() throws UnsupportedEncodingException {

        //given

        String[] args = new String[]{"test", "args"};

        //when
        new CmdsParser(testLogger, args);

        //then
        String expected = String.format("Start parser with arguments: %s%s", String.join(", ", args), System.lineSeparator());
        Assertions.assertEquals(loggerOutput.toString(utf8), expected);
    }

    @Test
    public void ShouldParseInputArguments() {
        //given
        CmdsParser parser = new CmdsParser(testLogger, new String[]{"--file=x.txt", "--display=3"});

        //when
        Map<Commands, String> result = parser.parse();

        //Then
        Assertions.assertEquals(result.get(Commands.FILE), "x.txt");
        Assertions.assertEquals(result.get(Commands.DISPLAY), "3");
    }

    @Test
    public void ShouldThrowIfArgumentsContainNoCommands() {
        //given
        CmdsParser parser = new CmdsParser(testLogger, new String[]{"file=x.txt", "--display=3"});

        //then
        Assertions.assertThrows(IllegalArgumentException.class, parser::parse);
    }

    @Test
    public void ShouldNotParseArgumentsIfEmptyInput() {
        //given
        CmdsParser parser = new CmdsParser(testLogger, new String[]{});

        //then
        Assertions.assertNull(parser.parse());
    }
}
